using PracticeCRUDandWedAPI;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();

// first way to register
builder.Services.AddDbContext<AppDbContext>();



var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapControllers();

app.Run();
