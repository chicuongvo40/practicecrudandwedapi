﻿using Microsoft.AspNetCore.Mvc;
using PracticeCRUDandWedAPI.Models;

namespace PracticeCRUDandWedAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly AppDbContext _context;

        public ProductsController(AppDbContext context)
        {
            _context = context;
        }
        // GET api/products
        [HttpGet]
        public ActionResult<IEnumerable<Product>> GetAllProducts() //
        {
            return _context.Products.ToList();
        }
        // GET api/products/{id}
        [HttpGet("{id}")]
        public ActionResult<Product> GetProductById(int id)
        {
            var product = _context.Products.Find(id);//truy vấn sản phẩm từ cơ sở dữ liệu

            if (product is null)
            {
                return NotFound(); // HTTP 404 Not Found 
            }

            return product;
        }

        // POST api/products
        [HttpPost]
        public ActionResult<Product> CreateProduct(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetProductById), new { id = product.Id }, product);
        }

        // PUT api/products/{id}
        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, Product updatedProduct)
        {
            var product = _context.Products.Find(id); 

            if (product is null)
            {
                return NotFound();
            }

            //cập nhật thông tin sản phẩm với thông tin được gửi lên từ khách hàng 
            product.Name = updatedProduct.Name;
            product.Price = updatedProduct.Price;
            
            _context.Products.Update(product); // cập nhật thông tin sản phẩm 
            _context.SaveChanges();// lưu các thay đổi của sản phẩm

            return NoContent();
        }

        // DELETE api/products/{id}
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var product = _context.Products.Find(id);

            if (product is null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            _context.SaveChanges();

            return NoContent();
        }
    }
}
