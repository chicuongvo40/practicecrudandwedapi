﻿using Microsoft.EntityFrameworkCore;
using PracticeCRUDandWedAPI.Models;

namespace PracticeCRUDandWedAPI
{
    public class AppDbContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connect = "Data Source=DESKTOP-ETC4R5E\\CHICUONG;Database=DIWithEFCoreAndAPI;User ID=sa;Password=123456;Trusted_Connection=True;TrustServerCertificate=True";
            optionsBuilder.UseSqlServer(connect);

        }
    }
}
